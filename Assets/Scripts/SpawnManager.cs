using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using System.Linq;
using Ordinalize;
public class SpawnManager : Singleton<SpawnManager>
{
    [SerializeField]
    private Countdown countdown;
    [SerializeField]
    private List<GameObject> spawnPrefabs;
    private bool isSpawning = false;
    private float timeToNextSpawn;
    [SerializeField]
    [Range(0f, 5f)]
    private float minTime = 0.2f;
    [SerializeField]
    [Range(0f, 5f)]
    private float maxTime = 0.3f;
    [SerializeField]
    private Transform spawnArea;
    [SerializeField]
    public List<Transform> targetAreas;
    private List<(GameObject, Transform)> spawnTargets;

    // Start is called before the first frame update
    void Start()
    {
        spawnTargets = spawnPrefabs.Zip(targetAreas, (first, second) => (first, second)).ToList();
        timeToNextSpawn = Random.Range(minTime, maxTime);
        countdown.CountdownEvent += onCountdownFinished;
    }

    private void onCountdownFinished(object sender, CountdownEventArgs e) {
        isSpawning = false;
    }

    public void StartSpawning() {
        isSpawning = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (!isSpawning) {
            return;
        }

        timeToNextSpawn -= Time.deltaTime;
        if (timeToNextSpawn <= 0 && countdown.timeLeft() >= maxTime) {
            Spawn();
            timeToNextSpawn = Random.Range(minTime, maxTime);
        }
    }

    public void SetSpawnDelay(float min, float max) {
        minTime = min;
        maxTime = max;
    }

    public void SetSpawnDelay(float multiplier) {
        minTime *= multiplier;
        maxTime *= multiplier;
    }

    private void Spawn() {

        var maxY = spawnArea.position.y + spawnArea.localScale.y / 2;
        var minY = spawnArea.position.y - spawnArea.localScale.y / 2;
        var maxX = spawnArea.position.x + spawnArea.localScale.x / 2;
        var minX = spawnArea.position.x - spawnArea.localScale.x / 2;
        var spawnTarget = spawnTargets.Random();
        var spawned = Instantiate(spawnTarget.Item1, new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY)), Quaternion.identity);
        spawned.GetComponent<Product>().targetArea = spawnTarget.Item2;
        ProductManager.Instance.Add(spawned);
    }
}
