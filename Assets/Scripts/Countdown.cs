using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownEventArgs {
    public CountdownEventArgs() {}
}


public class Countdown : MonoBehaviour
{
    public int totalSeconds = 10;
    private float counter;
    public delegate void CountdownEventHandler(object sender, CountdownEventArgs e);
    public event CountdownEventHandler CountdownEvent;
    public bool loop = false;
    bool stopped = true;
    [SerializeField]
    Slider slider;

    // Start is called before the first frame update
    void Awake()
    {
        counter = totalSeconds;
    }

    public void Restart() {
        counter = totalSeconds;
        stopped = false;
    }

    public void SetNewTime(int seconds, bool reset = false) {
        totalSeconds = seconds;
        if (reset) {
            Restart();
        }
    }
    public void SetNewTime(float multiplier, bool reset = false) {
        totalSeconds = (int)(totalSeconds * multiplier);
        if (reset) {
            Restart();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (stopped) {
            return;
        }

        if (counter > 0) {
            counter -= Time.deltaTime;
            slider.value = counter / totalSeconds;
        } else {
            stopped = true;
            CountdownEvent?.Invoke(this, new CountdownEventArgs());
            if (loop) {
                Restart();
                stopped = false;
            }
        }
    }

    internal float timeLeft() {
        return counter;
    }
}
