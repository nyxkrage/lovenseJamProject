using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Utils {

    public static class Util
    {
        public static void JsonPost(this MonoBehaviour monoBehaviour, Uri uri, object data, Action<string> action) {
            static IEnumerator enumerator(Uri uri, object data, Action<string> action) {
                var jsonData = JsonConvert.SerializeObject(data);
                UnityWebRequest www = UnityWebRequest.Post(uri, jsonData);
                byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonData);
                www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
                www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
                www.SetRequestHeader("Content-Type", "application/json");
                yield return www.SendWebRequest();

                if (www.result != UnityWebRequest.Result.Success) {
                    Debug.LogError(www.error);
                } else {
                    action(www.downloadHandler.text);
                }
            }
            monoBehaviour.StartCoroutine(enumerator(uri, data, action));
        }

        public static void Await(this MonoBehaviour monoBehaviour, float seconds, Action action) {
            static IEnumerator enumerator(float seconds, Action action) {
                yield return new WaitForSeconds(seconds);
                action();
            }
            var coroutine = monoBehaviour.StartCoroutine(enumerator(seconds, action));
            //monoBehaviour.StopCoroutine(coroutine);
        }
    }

    public static class StringExtensions {
        public static string Capitalize(this string str, int amount = 1) {
            return str switch {
                null => throw new ArgumentNullException(nameof(str)),
                "" => throw new ArgumentException($"{nameof(str)} cannot be empty", nameof(str)),
                _ => str.Substring(0,amount).ToUpper() + str.Substring(amount)
            };
        }
    }

    public static class ListExtensions {
        public static T Random<T>(this IList<T> list) {
            return list[UnityEngine.Random.Range(0, list.Count)];
        }
    }
}
