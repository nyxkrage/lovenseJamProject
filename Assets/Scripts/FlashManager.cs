using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class FlashManager : Singleton<FlashManager>
{
    [SerializeField]
    private Image flashPanel;
    [SerializeField]
    private Color successColor;
    [SerializeField]
    private Color failureColor;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float flashTime = 0.25f;


    public void Flash() {
        var productsLeft = ProductManager.Instance.Count();
        if (productsLeft == 0) {
            flashPanel.color = successColor;
        } else {
            LovenseManager.Instance.RunPattern(productsLeft);
            flashPanel.color = failureColor;
        }
        flashPanel.enabled = true;
        this.Await(flashTime, () => flashPanel.enabled = false);
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
