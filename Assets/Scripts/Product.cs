using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Collider2D))]
public class Product : MonoBehaviour
{
    public Transform targetArea;

    private Vector3 screenPoint;
    private Vector3 offset;

    void OnMouseDown() {
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag() {
        if (DayManager.Instance.isPaused) {
            return;
        }
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }
    private void OnMouseUp() {
        var x = transform.position.x;
        var y = transform.position.y;
        var maxY = targetArea.position.y + targetArea.localScale.y / 2;
        var minY = targetArea.position.y - targetArea.localScale.y / 2;
        var maxX = targetArea.position.x + targetArea.localScale.x / 2;
        var minX = targetArea.position.x - targetArea.localScale.x / 2;
        if (minX < x && x < maxX && minY < y && y < maxY) {
            ProductManager.Instance.Remove(gameObject);
        }
    }
}
