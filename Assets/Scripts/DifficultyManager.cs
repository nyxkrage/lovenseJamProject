using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyManager : Singleton<DifficultyManager> {
    [SerializeField]
    private Countdown countdown;
    [SerializeField]
    [Range(0, 2)]
    float countdownMultiplier = 1.05f;
    [SerializeField]
    [Range(0, 2)]
    float spawnDelayMultiplier = 0.8f;
    [SerializeField]
    [Range(0, 2)]
    float targetAreaScaleMultiplier = 0.95f;

    internal void IncreaseDifficulty() {
        // Decrease total time
        countdown.SetNewTime(countdownMultiplier);
        // Decrease spawn delay
        SpawnManager.Instance.SetSpawnDelay(spawnDelayMultiplier);
        // Decrease target areas size
        SpawnManager.Instance.targetAreas.ForEach((tf) => {
            tf.position = new Vector3(tf.position.x, tf.position.y - tf.localScale.y * (1f - targetAreaScaleMultiplier) / 2f, tf.position.z);
            tf.localScale *= targetAreaScaleMultiplier;
        });
    }
}
