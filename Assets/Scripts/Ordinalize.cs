using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Ordinalize {
    public static class IntegerExtensions {
        public static string Ordinalize(this int number) {
            return IntegerToWords.Convert(number);
        }
    }

    internal static class IntegerToWords {
        private static readonly string[] UnitsMap = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
        private static readonly string[] TensMap = { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

        private static readonly Dictionary<long, string> OrdinalExceptions = new Dictionary<long, string>
        {
            {1, "first"},
            {2, "second"},
            {3, "third"},
            {4, "fourth"},
            {5, "fifth"},
            {8, "eighth"},
            {9, "ninth"},
            {12, "twelfth"},
        };
        public static string Convert(long number, bool addAnd = true) {
            if (number == 0) {
                return GetUnitValue(0);
            }

            var parts = new List<string>();

            if (number > 0) {
                if (parts.Count != 0 && addAnd) {
                    parts.Add("and");
                }

                if (number < 20) {
                    parts.Add(GetUnitValue(number));
                } else {
                    var lastPart = TensMap[number / 10];
                    if ((number % 10) > 0) {
                        lastPart += string.Format("-{0}", GetUnitValue(number % 10));
                    } else {
                        lastPart = lastPart.TrimEnd('y') + "ieth";
                    }

                    parts.Add(lastPart);
                }
            } else {
                parts[parts.Count - 1] += "th";
            }

            var toWords = string.Join(" ", parts.ToArray());

            if (toWords.StartsWith("one", StringComparison.Ordinal)) {
                toWords = toWords.Remove(0, 4);
            }
            
            return toWords.Capitalize();
        }

        private static string GetUnitValue(long number) {
            if (OrdinalExceptions.TryGetValue(number, out string exceptionString)) {
                return exceptionString;
            } else {
                return UnitsMap[number] + "th";
            }
        }
    }
}
