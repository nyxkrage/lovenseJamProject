using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Http;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Text;
using Utils;
using TMPro;

public class LovenseManager : Singleton<LovenseManager>
{
    private Uri lovenseUri;
    private static readonly HttpClient client = new HttpClient();
    [SerializeField]
    Uri uri = new Uri("https://dev.nyx.xyz/qr");
    [SerializeField]
    Image qrImage;
    [SerializeField]
    GameObject authPanel;
    [SerializeField]
    Countdown coundown;
    [SerializeField]
    TMP_Text authText;

    public struct QrResponse {
        public string utoken;
        public string message;
    }

    public struct TokenRequest {
        public string utoken;
    }

    public struct LovensePattern {
        public string command;
        public string rule;
        public string strength;
        public double timeSec;
        public int apiVer;
    }

    public void RunPattern(int pulses) {
        var day = DayManager.Instance.GetDay();
        var pulse = $"{(int)(day * 2.5)};0;";
        var strength = new StringBuilder(pulse.Length * pulses).Insert(0, pulse, pulses).ToString();
        
        strength = strength.Substring(0, strength.Length - 1);

        var pattern = new LovensePattern {
            command = "Pattern",
            rule = "V:1;F:vr;S:100;",
            strength = strength,
            timeSec = Mathf.CeilToInt(pulses / 5f),
            apiVer = 1,
        };
        this.JsonPost(lovenseUri, pattern, (string s) => Debug.Log(s));
    }

    private void AckPattern() {
        var day = DayManager.Instance.GetDay();
        var pattern = new LovensePattern {
            command = "Pattern",
            rule = "V:1;F:vr;S:100;",
            strength = "5;0;0;0;0;0;0;0;0;0",
            timeSec = 1f,
            apiVer = 1,
        };
        this.JsonPost(lovenseUri, pattern, (string s) => Debug.Log(s));
    }

    public void Start() {
        DontDestroyOnLoad(gameObject);
        GetQrCode();
    }

    public void GetQrCode() {
        StartCoroutine(RequestQrCode());
    }

    private IEnumerator RequestQrCode() {
        UnityWebRequest www = UnityWebRequest.Post(uri, "{}");
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success) {
            Debug.LogError(www.error);
        } else {
            var res = JsonConvert.DeserializeObject<QrResponse>(www.downloadHandler.text);
            yield return StartCoroutine(DownloadQrImage(res));
        }
    }

    private IEnumerator DownloadQrImage(QrResponse res) {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(res.message);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success) {
            Debug.LogError(www.error);
        } else {
            qrImage.sprite = Sprite.Create(((DownloadHandlerTexture)www.downloadHandler).texture, qrImage.sprite.rect, qrImage.sprite.pivot);
            this.JsonPost(new Uri("https://dev.nyx.xyz/"), new TokenRequest { utoken = res.utoken }, (string s) => {
                lovenseUri = new Uri(s);
                qrImage.gameObject.SetActive(false);
                authText.text = "Please standby...";
                this.Await(0.25f, () => { 
                    AckPattern();
                    authPanel.SetActive(false);
                    SpawnManager.Instance.StartSpawning();
                    coundown.Restart();
                });
            });
        }
    }
}
