using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Ordinalize;
using Utils;

public class DayManager : Singleton<DayManager> {
    private int dayCounter = 0;
    public bool isPaused { get; private set; } = false;
    [SerializeField]
    private TMP_Text text;
    [SerializeField]
    private Countdown countdown;

    void Start() {
        countdown.CountdownEvent += onCountdownFinished;
    }

    public int GetDay() {
        return dayCounter;
    }

    private void onCountdownFinished(object sender, CountdownEventArgs e) {
        isPaused = true;
        IncreaseDay();
    }

    public void IncreaseDay(int amount = 1) {
        dayCounter += amount;
        text.text = $"{dayCounter.Ordinalize()} day";
        text.enabled = true;
        DifficultyManager.Instance.IncreaseDifficulty();
        // TODO: Fade rather than instantly switching
        FlashManager.Instance.Flash();
        ProductManager.Instance.ClearSorted();
        this.Await(3.0f, () => text.enabled = false);
        this.Await(3.5f, () => {
            SpawnManager.Instance.StartSpawning();
            countdown.Restart();
            isPaused = false;
        });
    }
}
