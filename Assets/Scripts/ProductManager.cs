using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductManager : Singleton<ProductManager>
{
    private List<GameObject> unsorted = new List<GameObject>();
    private List<GameObject> sorted = new List<GameObject>();

    public int Count() {
        return unsorted.Count;
    }

    public void ClearSorted() {
        sorted.ForEach((go) => Destroy(go));
    }

    public void Remove(GameObject go) {
        unsorted.Remove(go);
        sorted.Add(go);
    }
    
    public void Add(GameObject go) {
        unsorted.Add(go);
    }
}
